
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	sessionId = getSessionId();
	
	var d = new Date();
	d.setDate(d.getDate() - 5);
	var dd = d.getDate();
	var mm = d.getMonth() + 1;
	var yyyy = d.getFullYear();
	if(dd < 10){
	    dd = '0' + dd;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	var date1 = yyyy + '-' + mm + '-' + dd;
	
	var d = new Date();
	d.setDate(d.getDate() - 4);
	var dd = d.getDate();
	var mm = d.getMonth() + 1;
	var yyyy = d.getFullYear();
	if(dd < 10){
	    dd = '0' + dd;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	var date2 = yyyy + '-' + mm + '-' + dd;
	
	var d = new Date();
	d.setDate(d.getDate() - 3);
	var dd = d.getDate();
	var mm = d.getMonth() + 1;
	var yyyy = d.getFullYear();
	if(dd < 10){
	    dd = '0' + dd;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	var date3 = yyyy + '-' + mm + '-' + dd;
	
	var d = new Date();
	d.setDate(d.getDate() - 2);
	var dd = d.getDate();
	var mm = d.getMonth() + 1;
	var yyyy = d.getFullYear();
	if(dd < 10){
	    dd = '0' + dd;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	var date4 = yyyy + '-' + mm + '-' + dd;
	
	var d = new Date();
	d.setDate(d.getDate() - 1);
	var dd = d.getDate();
	var mm = d.getMonth() + 1;
	var yyyy = d.getFullYear();
	if(dd < 10){
	    dd = '0' + dd;
	} 
	if(mm < 10){
	    mm = '0' + mm;
	} 
	var date5 = yyyy + '-' + mm + '-' + dd;

	if(stPacienta == 1) {
		var firstName = 'Tjaša';
		var lastName = 'Gal';
	    var dob = '1989-12-13' + "T00:00:00.000Z";
	    
	    var dates = [date1, date2, date3, date4, date5];
	    var temperatures = ['37', '36.8', '36.5', '36.4', '36.9'];
	    var sysbps = ['100', '106', '97', '94', '92'];
	    var diabps = ['66', '67', '62', '72', '69'];
	    var sats = ['97', '98', '98', '99', '98'];
	}
	else if(stPacienta == 2) {
		var firstName = 'Jakob';
		var lastName = 'Muster';
	    var dob = '1948-01-24' + "T00:00:00.000Z";
	    
	    var dates = ['2018-02-02', '2018-02-04', '2018-02-07', date3, date4];
	    var temperatures = ['39', '39.4', '39.9', '38', '38.2'];
	    var sysbps = ['123', '126', '132', '130', '117'];
	    var diabps = ['92', '87', '84', '85', '79'];
	    var sats = ['91', '92', '94', '93', '96'];
	}
	else {
		var firstName = 'Jožef';
		var lastName = 'Rogelj';
	    var dob = '1975-05-02' + "T00:00:00.000Z";
	    
	    var dates = ['2018-01-01', '2018-01-02', '2018-01-03', '2018-01-04', '2018-01-05'];
	    var temperatures = ['34', '35', '36.9', '40.8', '42.1'];
	    var sysbps = ['86', '81', '73', '74', '70'];
	    var diabps = ['55', '50', '48', '44', '46'];
	    var sats = ['99', '97', '98', '96', '99'];
	}

	ehrId = "";

	if (firstName && lastName && dob) {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var a = $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    async: false,
		    success: function (data) {
		        ehrId = data.ehrId;
		        
		        var partyData = {
		            firstNames: firstName,
		            lastNames: lastName,
		            dateOfBirth: dob,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	
		                }
		            },
		            error: function(err) {
		            	//Failed
		            }
		        });
		        
		        return ehrId;
		    }
		});
	}
	cont(ehrId, 0, dates, temperatures, sysbps, diabps, sats)
	return ehrId;
}
	
function cont(ehrId, i, dates, temperatures, sysbps, diabps, sats) {
	    var ehr = ehrId;
	    var date = dates[i];
	    var temperature = temperatures[i];
	    var sysbp = sysbps[i];
	    var diabp = diabps[i];
	    var oxygen = sats[i];
	    
	    if(ehr && date && temperature && sysbp && diabp && oxygen) {
	        date = date  + "T00:00:00.000Z";
	        
	        $.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			
			var data = {
			    "ctx/language": "en",
			    "ctx/territory": "SI",
			    "ctx/time": date,
			   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperature,
			    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
			    "vital_signs/blood_pressure/any_event/systolic": sysbp,
			    "vital_signs/blood_pressure/any_event/diastolic": diabp,
			    "vital_signs/indirect_oximetry:0/spo2|numerator": oxygen
			};
			
			var parameters = {
			    ehrId: ehr,
			    templateId: 'Vital Signs',
			    format: 'FLAT'
			};
			
			$.ajax({
			    url: baseUrl + "/composition?" + $.param(parameters),
			    type: 'POST',
			    contentType: 'application/json',
			    data: JSON.stringify(data),
			    async: false,
			    success: function (result) {
	                if(i < dates.length - 1) {
	                	cont(ehrId, i + 1,  dates, temperatures, sysbps, diabps, sats);
	                }
			    },
			    error: function(err) {
			        
			    }
			});
	    }
	}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
var highestTemp = 0;
var lowestTemp = 0;
var weatherNotifs = [];
var otherNotifs = [];
var criticalNotifs = [];
var accurate = 1;
function vreme(data) {
	highestTemp = data.query.results.channel.item.forecast[0].high;
	lowestTemp = data.query.results.channel.item.forecast[0].low;
};

function readData(ehr) {
    sessionId = getSessionId();
    
    weatherNotifs = [];
    otherNotifs = [];
    criticalNotifs = [];
    accurate = 1;
    
    if(ehr) {
        $.ajax({
		    url: baseUrl + "/demographics/ehr/" + ehr + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
				if(party.length == 0) {
					$('#userSelect').show();
					$('#userMsg').html('Za izbranega uporabnika ni podatkov.');
					
					$('#timelineMeasurements').hide();
					
					return;
				}
				
				$('#patName').html(party.firstNames + ' ' + party.lastNames);
				
				var year = parseInt(String(party.dateOfBirth).substring(0, 4));
				var month = parseInt(String(party.dateOfBirth).substring(5, 7));
				var day = parseInt(String(party.dateOfBirth).substring(8, 10));
				
				var dt = new Date(month + '/' + day + '/' + year);
				var today = new Date();
				var years = today.getFullYear() - dt.getFullYear();
				
				dt.setFullYear(today.getFullYear());
				
				if (today < dt)  {
				    years--;
				}
				
				$('#patAge').html(years + ' let');
				$('#birthDate').html(day + '. ' + month + '. ' + year);
				
				
        $.ajax({
  		    url: baseUrl + "/view/" + ehr + "/" + "body_temperature",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	if (res.length > 0) {
		    		google.charts.load('current', {'packages':['corechart']});
					google.charts.setOnLoadCallback(drawChart);
					
					function drawChart() {
						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Measurement Date');
						data.addColumn('number', 'Temperature');
						data.addColumn({type:'string', role:'style'});
						
						var temperatures = [];
						for(var i in res) {
							var year = parseInt(String(res[res.length - i - 1].time).substring(0, 4));
							var month = parseInt(String(res[res.length - i - 1].time).substring(5, 7));
							var day = parseInt(String(res[res.length - i - 1].time).substring(8, 10));
							
							temperatures.push([new Date(year, month, day), res[res.length - i - 1].temperature, 'stroke-color: #1976d2; fill-color: #1976d2;']);
							
							var year = parseInt(String(res[i].time).substring(0, 4));
							var month = parseInt(String(res[i].time).substring(5, 7));
							var day = parseInt(String(res[i].time).substring(8, 10));
							
							if(i == 0) {
								document.getElementById("bodyTemp").innerHTML = (Math.round(parseFloat(res[i].temperature) * 10) / 10) + " " + res[i].unit;
								
								if((Math.round(parseFloat(res[i].temperature) * 10) / 10) > 39) {
									criticalNotifs.push("kritično visoka telesna temperatura");
									if(highestTemp >= 35) {
										criticalNotifs.push("visoka verjetnost sončne kapi");
									}
								}
								else if((Math.round(parseFloat(res[i].temperature) * 10) / 10) < 34) {
									criticalNotifs.push("kritično nizka telesna temperatura");
								}
								else if((Math.round(parseFloat(res[i].temperature) * 10) / 10) > 37.5) {
									otherNotifs.push("povišano telesno temperaturo");
								}
								else if((Math.round(parseFloat(res[i].temperature) * 10) / 10) < 35.5) {
									otherNotifs.push("znižano telesno temperaturo");
								}
								
								
								if(highestTemp >= 26 && (Math.round(parseFloat(res[i].temperature) * 10) / 10) > 37) {
									weatherNotifs.push("zvišanje temperature nad normalno raven");
								}
								else if(lowestTemp <= 3 && (Math.round(parseFloat(res[i].temperature) * 10) / 10) < 36) {
									weatherNotifs.push("znižanje temperature pod normalno raven");
								}
								
								$('#first').attr('data-date', day + '. ' + month + '. ' + year);
								$('#first').attr('data-temp', (Math.round(parseFloat(res[i].temperature) * 10) / 10));
								$('#first').html('<span>' + day + '. ' + month + '. ' + year + '</span>');
								
								var dt = new Date(month + '/' + day + '/' + year);
								var today = new Date();
								var three = new Date();
								three.setDate(three.getDate()-3);
								
								if(three > dt) {
									accurate = 0;
								}
								else if(today < dt) {
									accurate = -1;
								}
							}
							else if(i == 1) {
								$('#second').attr('data-date', day + '. ' + month + '. ' + year);
								$('#second').attr('data-temp', (Math.round(parseFloat(res[i].temperature) * 10) / 10));
								$('#second').html('<span>' + day + '. ' + month + '. ' + year + '</span>');
							}
							else if(i == 2) {
								$('#third').attr('data-date', day + '. ' + month + '. ' + year);
								$('#third').attr('data-temp', (Math.round(parseFloat(res[i].temperature) * 10) / 10));
								$('#third').html('<span>' + day + '. ' + month + '. ' + year + '</span>');
							}
							else if(i == 3) {
								$('#fourth').attr('data-date', day + '. ' + month + '. ' + year);
								$('#fourth').attr('data-temp', (Math.round(parseFloat(res[i].temperature) * 10) / 10));
								$('#fourth').html('<span>' + day + '. ' + month + '. ' + year + '</span>');
							}
							else if(i == 4) {
								$('#fifth').attr('data-date', day + '. ' + month + '. ' + year);
								$('#fifth').attr('data-temp', (Math.round(parseFloat(res[i].temperature) * 10) / 10));
								$('#fifth').html('<span>' + day + '. ' + month + '. ' + year + '</span>');
							}
						}
						
						data.addRows(temperatures);
						
						var options = {
						  chartArea: {width: '100%', height: '100%'},
						  animation:{
						    duration: 1000,
						    easing: 'out',
						    startup: 'true'
						  },
						  backgroundColor: {
						    fill: '#2196f3'
						  },
						  legend: {
						    position: 'none'
						  },
						  hAxis: {
						    textPosition: 'none',
						    gridlines: {
						    	color: 'transparent'
						    }
						  },
						  vAxis: {
						    textPosition: 'none',
						    gridlines: {
						      color: 'transparent'
						    },
						    minValue: 0,
			                viewWindow: {
			                    min: 0
			                }
						  }
						};
						
						var chart = new google.visualization.AreaChart(document.getElementById('temperature'));
						chart.draw(data, options);
					}
		    	}
		
		$.ajax({
  		    url: baseUrl + "/view/" + ehr + "/" + "spO2",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	if (res.length > 0) {
		    		google.charts.load('current', {'packages':['corechart']});
					google.charts.setOnLoadCallback(drawChart);
					
					function drawChart() {
						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Measurement Date');
						data.addColumn('number', 'BMI');
						data.addColumn({type:'string', role:'style'});
						
						var sats = [];
						
						if(res.length < 5) {
							$('#second').show();
							$('#cndu').show();
							$('#tnd').show();
							$('#cndl').show();
							
							$('#third').show();
							$('#crdu').show();
							$('#trd').show();
							$('#crdl').show();
							
							$('#fourth').show();
							$('#cthu').show();
							$('#tth').show();
							$('#cthl').show();
						
							$('#fifth').hide();
							$('#cfu').hide();
							$('#tf').hide();
							$('#cfl').hide();
						}
						if(res.length < 4) {
							$('#second').show();
							$('#cndu').show();
							$('#tnd').show();
							$('#cndl').show();
							
							$('#third').show();
							$('#crdu').show();
							$('#trd').show();
							$('#crdl').show();
						
							$('#fourth').hide();
							$('#cthu').hide();
							$('#tth').hide();
							$('#cthl').hide();
						}
						if(res.length < 3) {
							$('#second').show();
							$('#cndu').show();
							$('#tnd').show();
							$('#cndl').show();
						
							$('#third').hide();
							$('#crdu').hide();
							$('#trd').hide();
							$('#crdl').hide();
						}
						if(res.length < 2) {
							$('#second').hide();
							$('#cndu').hide();
							$('#tnd').hide();
							$('#cndl').hide();
						}
						if(res.length >= 5) {
							$('#second').show();
							$('#cndu').show();
							$('#tnd').show();
							$('#cndl').show();
							
							$('#third').show();
							$('#crdu').show();
							$('#trd').show();
							$('#crdl').show();
							
							$('#fourth').show();
							$('#cthu').show();
							$('#tth').show();
							$('#cthl').show();
							
							$('#fifth').show();
							$('#cfu').show();
							$('#tf').show();
							$('#cfl').show();
						}
						
						for(var i in res) {
							var sat = parseInt(res[res.length - i - 1].spO2);
							
							var year = parseInt(String(res[res.length - i - 1].time).substring(0, 4));
							var month = parseInt(String(res[res.length - i - 1].time).substring(5, 7));
							var day = parseInt(String(res[res.length - i - 1].time).substring(8, 10));
							
							sats.push([new Date(year, month, day), sat, 'stroke-color: #689f38; fill-color: #689f38;']);
							
							if(i == 0) {
								document.getElementById("satI").innerHTML = res[i].spO2 + " %";
								
								if(parseInt(res[i].spO2) <= 92) {
									criticalNotifs.push("kritično nizka raven kisika v krvi");
								}
								else if(parseInt(res[i].spO2) < 95) {
									otherNotifs.push("znižano raven kisika v krvi");
								}
								
								if(highestTemp >= 26 && parseInt(res[i].spO2) <= 96) {
									weatherNotifs.push("znižanje ravni kiska v krvi pod normalno raven");
								}
								
								$('#first').attr('data-sat', res[i].spO2);
							}
							else if(i == 1) {
								$('#second').attr('data-sat', res[i].spO2);
							}
							else if(i == 2) {
								$('#third').attr('data-sat', res[i].spO2);
							}
							else if(i == 3) {
								$('#fourth').attr('data-sat', res[i].spO2);
							}
							else if(i == 4) {
								$('#fifth').attr('data-sat', res[i].spO2);
							}
						}
						
						data.addRows(sats);
						
						var options = {
						  chartArea: {width: '100%', height: '100%'},
						  animation:{
						    duration: 1000,
						    easing: 'out',
						    startup: 'true'
						  },
						  backgroundColor: {
						    fill: 'rgb(139, 195, 74)'
						  },
						  legend: {
						    position: 'none'
						  },
						  hAxis: {
						    textPosition: 'none',
						    gridlines: {
						    	color: 'transparent'
						    }
						  },
						  vAxis: {
						    textPosition: 'none',
						    gridlines: {
						      color: 'transparent'
						    },
						    minValue: 0,
			                viewWindow: {
			                    min: 0
			                }
						  }
						};
						
						var chart = new google.visualization.AreaChart(document.getElementById('sat'));
						chart.draw(data, options);
					}
		    	}
		
		$.ajax({
  		    url: baseUrl + "/view/" + ehr + "/" + "blood_pressure",
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	if (res.length > 0) {
		    	    google.charts.load('current', {'packages':['corechart']});
					google.charts.setOnLoadCallback(drawChart);
					
					function drawChart() {
						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Measurement Date');
						data.addColumn('number', 'LBP');
						data.addColumn({type:'string', role:'style'});
						data.addColumn('number', 'HBP');
						data.addColumn({type:'string', role:'style'});
						
			    		var bps = [];
						for(var i in res) {
							var year = parseInt(String(res[res.length - i - 1].time).substring(0, 4));
							var month = parseInt(String(res[res.length - i - 1].time).substring(5, 7));
							var day = parseInt(String(res[res.length - i - 1].time).substring(8, 10));
							
							bps.push([new Date(year, month, day), res[res.length - i - 1].systolic, 'stroke-color: #e53935; fill-color: #e53935;', res[res.length - i - 1].diastolic, 'stroke-color: #d32f2f; fill-color: #d32f2f;']);
							
							if(i == 0) {
								document.getElementById("systolicBP").innerHTML = parseInt(res[i].systolic) + " " + res[i].unit;
								document.getElementById("diastolicBP").innerHTML = parseInt(res[i].diastolic) + " " + res[i].unit;
								
								if(parseInt(res[i].systolic) < 75) {
									criticalNotifs.push("sistolični krvni pritisk je kritično nizek");
								}
								else if(parseInt(res[i].systolic) > 140) {
									criticalNotifs.push("sistolični krvni pritisk je kritično visok");
								}
								else if(parseInt(res[i].systolic) < 90) {
									otherNotifs.push("znižan sistolični krvni pritisk");
								}
								else if(parseInt(res[i].systolic) > 120) {
									otherNotifs.push("zvišan sistolični krvni pritisk");
								}
								
								if(highestTemp >= 26 && parseInt(res[i].systolic) < 95) {
									weatherNotifs.push("znižanje sistoličnega krvnega pritiska pod normalno raven");
								}
								else if(lowestTemp <= 3 && parseInt(res[i].systolic) > 115) {
									weatherNotifs.push("zvišanje sistoličnega krvnega pritiska nad normalno raven");
								}
								
								
								if(parseInt(res[i].diastolic) < 45) {
									criticalNotifs.push("diastolični krvni pritisk je kritično nizek");
								}
								else if(parseInt(res[i].diastolic) > 90) {
									criticalNotifs.push("diastolični krvni pritisk je kritično visok");
								}
								else if(parseInt(res[i].diastolic) < 60) {
									otherNotifs.push("znižan diastolični krvni pritisk");
								}
								else if(parseInt(res[i].diastolic) > 90) {
									otherNotifs.push("zvišan diastolični krvni pritisk");
								}
								
								if(highestTemp >= 26 && parseInt(res[i].diastolic) < 65) {
									weatherNotifs.push("znižanje diastoličnega krvnega pritiska pod normalno raven");
								}
								else if(lowestTemp <= 3 && parseInt(res[i].diastolic) > 75) {
									weatherNotifs.push("zvišanje diastoličnega krvnega pritiska nad normalno raven");
								}
								
								
								$('#first').attr('data-sys', parseInt(res[i].systolic));
								$('#first').attr('data-dia', parseInt(res[i].diastolic));
							}
							else if(i == 1) {
								$('#second').attr('data-sys', parseInt(res[i].systolic));
								$('#second').attr('data-dia', parseInt(res[i].diastolic));
							}
							else if(i == 2) {
								$('#third').attr('data-sys', parseInt(res[i].systolic));
								$('#third').attr('data-dia', parseInt(res[i].diastolic));
							}
							else if(i == 3) {
								$('#fourth').attr('data-sys', parseInt(res[i].systolic));
								$('#fourth').attr('data-dia', parseInt(res[i].diastolic));
							}
							else if(i == 4) {
								$('#fifth').attr('data-sys', parseInt(res[i].systolic));
								$('#fifth').attr('data-dia', parseInt(res[i].diastolic));
							}
						}
						
						data.addRows(bps);
						
						var options = {
						  chartArea: {width: '100%', height: '100%'},
						  animation:{
						    duration: 1000,
						    easing: 'out',
						    startup: 'true'
						  },
						  backgroundColor: {
						    fill: '#f44336'
						  },
						  legend: {
						    position: 'none'
						  },
						  hAxis: {
						    textPosition: 'none',
						    gridlines: {
						    	color: 'transparent'
						    }
						  },
						  vAxis: {
						    textPosition: 'none',
						    gridlines: {
						      color: 'transparent'
						    },
						    minValue: 0,
			                viewWindow: {
			                    min: 0
			                }
						  }
						};
						
						var chart = new google.visualization.AreaChart(document.getElementById('bloodPressure'));
						chart.draw(data, options);
						
						weatherRisks();
					}
		    	}
		    },
		    error: function(err) {
		    	// Napaka: JSON.parse(err.responseText).userMessage
		    }
		});
		
		    },
		    error: function(err) {
		    	// Napaka: JSON.parse(err.responseText).userMessage
		    }
		});
		
		    },
		    error: function(err) {
		    	// Napaka: JSON.parse(err.responseText).userMessage
		    }
		});
		
	    	}
        });
    }
    else {
    	$('#userData').hide();
    	$('#userSelect').show();
    	$('#weatherCard').hide();
    	$('#timelineMeasurements').hide();
        $('#userMsg').html('Za izbranega uporabnika ni podatkov.');
    }
}

function weatherRisks() {
	$('#userSelect').hide();
	$('#timelineMeasurements').show();
	$('#first').click();
	$('#otherRisks').html('');
	$('#othrRisks').html('');
	if(accurate == 0) {
		$('#inaccurate').html('<strong>Podatki o meritvah so stari več kot 3 dni, zato napovedi morda niso točne.</strong>');
	}
	else if(accurate == 1) {
		$('#inaccurate').html('');
	}
	else {
		$('#inaccurate').html('<strong>Napoved ni zanesljiva. Meritev ne more biti novejša od današnjega dne.</strong>');
	}
	
	if(weatherNotifs.length > 0 || otherNotifs.length > 0 || criticalNotifs.length > 0) {
		if(criticalNotifs.length > 0) {
			$('#weatherC').removeClass('bg-info');
			$('#weatherC').removeClass('bg-warning');
			$('#weatherC').removeClass('orange');
			$('#weatherC').removeClass('bg-success');
			$('#weatherC').addClass('bg-danger');
			$('#weatherTitle').html('Vaše zdravstveno stanje je kritično!');
			$('#weatherForecast').html('Takoj obiščite zdravnika zaradi naslednjih stanj:');
			$('#weatherIcon').html('<i class="fas fa-exclamation-triangle weather-icon"></i>');
			for(var i in criticalNotifs) {
				if(i == 0) {
					$('#weatherRisks').html('<li>' + criticalNotifs[i] + '</li>');
				}
				else {
					var wN = $('#weatherRisks').html();
					$('#weatherRisks').html(wN + '<li>' + criticalNotifs[i] + '</li>');
				}
			}
			for(var i in otherNotifs) {
				var wN = $('#weatherRisks').html();
				$('#weatherRisks').html(wN + '<li>' + otherNotifs[i] + '</li>');
			}
			$('#weatherCard').show();
		}
		else if(highestTemp >= 26 && weatherNotifs.length > 0) {
			$('#weatherC').removeClass('bg-info');
			$('#weatherC').removeClass('bg-warning');
			$('#weatherC').removeClass('bg-danger');
			$('#weatherC').removeClass('bg-success');
			$('#weatherC').addClass('orange');
			$('#weatherTitle').html('Jutri bo vroče: do največ ' + highestTemp + ' °C');
			$('#weatherForecast').html('Glede na vaše zadnje meritve so z jutrišnjo vremensko napovedjo povezana naslednja tveganja:');
			$('#weatherIcon').html('<i class="fas fa-sun weather-icon"></i>');
			for(var i in weatherNotifs) {
				if(i == 0) {
					$('#weatherRisks').html('<li>' + weatherNotifs[i] + '</li>');
				}
				else {
					var wN = $('#weatherRisks').html();
					$('#weatherRisks').html(wN + '<li>' + weatherNotifs[i] + '</li>');
				}
			}
			if(otherNotifs.length > 0) {
				$('#otherRisks').html('Poleg tega imate trenutno naslednje zdravstvene težave:');
				for(var i in otherNotifs) {
					if(i == 0) {
						$('#othrRisks').html('<li>' + otherNotifs[i] + '</li>');
					}
					else {
						var w = $('#othrRisks').html();
						$('#othrRisks').html(w + '<li>' + otherNotifs[i] + '</li>');
					}
				}
			}
			$('#weatherCard').show();
		}
		else if(lowestTemp <= 3 && weatherNotifs.length > 0) {
			$('#weatherC').removeClass('bg-danger');
			$('#weatherC').removeClass('bg-warning');
			$('#weatherC').removeClass('orange');
			$('#weatherC').removeClass('bg-success');
			$('#weatherC').addClass('bg-info');
			$('#weatherTitle').html('Jutri bo mrzlo: do najmanj ' + lowestTemp + ' °C');
			$('#weatherIcon').html('<i class="fas fa-snowflake weather-icon"></i>');
			$('#weatherForecast').html('Glede na vaše zadnje meritve so z jutrišnjo vremensko napovedjo povezana naslednja tveganja:');
			$('#weatherRisks').html(' ');
			for(var i in weatherNotifs) {
				if(i == 0) {
					$('#weatherRisks').html('<li>' + weatherNotifs[i] + '</li>');
				}
				else {
					var wN = $('#weatherRisks').html();
					$('#weatherRisks').html(wN + '<li>' + weatherNotifs[i] + '</li>');
				}
			}
			if(otherNotifs.length > 0) {
				$('#otherRisks').html('Poleg tega imate trenutno naslednje zdravstvene težave:');
				for(var i in otherNotifs) {
					if(i == 0) {
						$('#othrRisks').html('<li>' + otherNotifs[i] + '</li>');
					}
					else {
						var w = $('#othrRisks').html();
						$('#othrRisks').html(w + '<li>' + otherNotifs[i] + '</li>');
					}
				}
			}
			$('#weatherCard').show();
		}
		else if(otherNotifs.length != 0) {
			$('#weatherC').removeClass('bg-info');
			$('#weatherC').removeClass('bg-danger');
			$('#weatherC').removeClass('orange');
			$('#weatherC').removeClass('bg-success');
			$('#weatherC').addClass('bg-warning');
			$('#weatherTitle').html('Jutrišnje vreme ne bo bistveno vplivalo na vaše zdravje.');
			$('#weatherIcon').html('<i class="fas fa-comment weather-icon"></i>');
			$('#weatherForecast').html('Kljub temu niste povsem zdravi, saj imate:');
			$('#weatherRisks').html(' ');
			for(var i in otherNotifs) {
				if(i == 0) {
					$('#weatherRisks').html('<li>' + otherNotifs[i] + '</li>');
				}
				else {
					var wN = $('#weatherRisks').html();
					$('#weatherRisks').html(wN + '<li>' + otherNotifs[i] + '</li>');
				}
			}
			$('#weatherCard').show();
		}
	}
	else {
		$('#weatherC').removeClass('bg-info');
		$('#weatherC').removeClass('bg-danger');
		$('#weatherC').removeClass('orange');
		$('#weatherC').removeClass('bg-warning');
		$('#weatherC').addClass('bg-success');
		$('#weatherTitle').html('Jutrišnje vreme ne bo bistveno vplivalo na vaše zdravje.');
		$('#weatherIcon').html('<i class="far fa-smile weather-icon"></i>');
		$('#weatherForecast').html('Poleg tega nimate zdravstvenih težav.');
		$('#weatherRisks').html(' ');
		$('#weatherCard').show();
	}
}

function addMeasurement() {
    sessionId = getSessionId();
    
    var ehr = $("#ehr").val();
    var date = $("#dm").val();
    var temperature = $("#bodyTemperature").val();
    var sysbp = $("#sys-bp").val();
    var diabp = $("#dia-bp").val();
    var oxygen = $("#oxygen").val();
    
    if(ehr && date && temperature && sysbp && diabp && oxygen) {
        date = date  + "T00:00:00.000Z";
        
        var year = parseInt(date.substring(0, 4));
		var month = parseInt(date.substring(5, 7));
		var day = parseInt(date.substring(8, 10));
        
        var dt = new Date(month + '/' + day + '/' + year);
		var today = new Date();
		
		if(today < dt) {
			$("#measurementMsg").html("Datum meritve mora biti starejši ali enak današnjemu.");
	        $("#addMeasurementCard").show();
	        
	        return;
		}
        
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		var data = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": date,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperature,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sysbp,
		    "vital_signs/blood_pressure/any_event/diastolic": diabp,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": oxygen
		};
		
		var parameters = {
		    ehrId: ehr,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parameters),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(data),
		    success: function (result) {
                $("#measurementMsg").html("Meritev je bila uspešno shranjena.");
		        $("#addMeasurementCard").show();
		    },
		    error: function(err) {
		        $("#measurementMsg").html("Napaka '" + JSON.parse(err.responseText).userMessage + "'.");
		        $("#addMeasurementCard").show();
		    }
		});
    }
    else {
        $("#measurementMsg").html("Prosimo, vnesite zahtevane podatke.");
		$("#addMeasurementCard").show();
    }
}

$(document).ready(function() {
    $('#selectUser').change(function autoE() {
    	$('#userData').show();
    	var ehr = $('#selectUser').val();
    	$('#userEhr').html(ehr);
    	$('#ehr').val(ehr);
        readData(ehr);
    }); 
    
    $('#confirmEHR').click(function manualE() {
    	$('#userData').show();
    	var ehr = $('#manualEHR').val();
    	$('#userEhr').html(ehr);
    	$('#ehr').val(ehr);
        readData(ehr);
    });
    
    $('#first').click(function first() {
    	var date = $('#first').attr('data-date');
    	var temp = $('#first').attr('data-temp');
    	var sys = $('#first').attr('data-sys');
    	var dia = $('#first').attr('data-dia');
    	var sat = $('#first').attr('data-sat');
    	
    	$('#measurementDate').html(date);
    	$('#tempDetail').html(temp + '°C');
    	$('#bpDetail').html(sys + ' / ' + dia + ' mm[Hg]');
    	$('#satDetail').html(sat + '%');
    	
    	if(temp > 37.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je višja od normalne!');
    	}
    	else if(temp < 35.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je normalna.');
    	}
    	
    	if(sys > 120) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(sys < 90) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(dia > 90) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(dia < 60) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(sat < 95) {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je normalna.');
    	}
    });
    
    $('#second').click(function second() {
    	var date = $('#second').attr('data-date');
    	var temp = $('#second').attr('data-temp');
    	var sys = $('#second').attr('data-sys');
    	var dia = $('#second').attr('data-dia');
    	var sat = $('#second').attr('data-sat');
    	
    	$('#measurementDate').html(date);
    	$('#tempDetail').html(temp + '°C');
    	$('#bpDetail').html(sys + ' / ' + dia + ' mm[Hg]');
    	$('#satDetail').html(sat + '%');
    	
    	if(temp > 37.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je višja od normalne!');
    	}
    	else if(temp < 35.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je normalna.');
    	}
    	
    	if(sys > 120) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(sys < 90) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(dia > 90) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(dia < 60) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(sat < 95) {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je normalna.');
    	}
    });
    
    $('#third').click(function third() {
    	var date = $('#third').attr('data-date');
    	var temp = $('#third').attr('data-temp');
    	var sys = $('#third').attr('data-sys');
    	var dia = $('#third').attr('data-dia');
    	var sat = $('#third').attr('data-sat');
    	
    	$('#measurementDate').html(date);
    	$('#tempDetail').html(temp + '°C');
    	$('#bpDetail').html(sys + ' / ' + dia + ' mm[Hg]');
    	$('#satDetail').html(sat + '%');
    	
    	if(temp > 37.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je višja od normalne!');
    	}
    	else if(temp < 35.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je normalna.');
    	}
    	
    	if(sys > 120) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(sys < 90) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(dia > 90) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(dia < 60) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(sat < 95) {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je normalna.');
    	}
    });
    
    $('#fourth').click(function fourth() {
    	var date = $('#fourth').attr('data-date');
    	var temp = $('#fourth').attr('data-temp');
    	var sys = $('#fourth').attr('data-sys');
    	var dia = $('#fourth').attr('data-dia');
    	var sat = $('#fourth').attr('data-sat');
    	
    	$('#measurementDate').html(date);
    	$('#tempDetail').html(temp + '°C');
    	$('#bpDetail').html(sys + ' / ' + dia + ' mm[Hg]');
    	$('#satDetail').html(sat + '%');
    	
    	if(temp > 37.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je višja od normalne!');
    	}
    	else if(temp < 35.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je normalna.');
    	}
    	
    	if(sys > 120) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(sys < 90) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(dia > 90) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(dia < 60) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(sat < 95) {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je normalna.');
    	}
    });
    
    $('#fifth').click(function fifth() {
    	var date = $('#fifth').attr('data-date');
    	var temp = $('#fifth').attr('data-temp');
    	var sys = $('#fifth').attr('data-sys');
    	var dia = $('#fifth').attr('data-dia');
    	var sat = $('#fifth').attr('data-sat');
    	
    	$('#measurementDate').html(date);
    	$('#tempDetail').html(temp + '°C');
    	$('#bpDetail').html(sys + ' / ' + dia + ' mm[Hg]');
    	$('#satDetail').html(sat + '%');
    	
    	if(temp > 37.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je višja od normalne!');
    	}
    	else if(temp < 35.5) {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#tempDesc').html('Telesna temperatura ob tej meritvi je normalna.');
    	}
    	
    	if(sys > 120) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(sys < 90) {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#sysDesc').html('Sistolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(dia > 90) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je višji od normalnega!');
    	}
    	else if(dia < 60) {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je nižji od normalnega!');
    	}
    	else {
    		$('#diaDesc').html('Diastolični krvni pritisk ob tej meritvi je normalen.');
    	}
    	
    	if(sat < 95) {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je nižja od normalne!');
    	}
    	else {
    		$('#satDesc').html('Raven kisika v krvi ob tej meritvi je normalna.');
    	}
    });
    
    $('#ljubljana').click(function ljubljana() {
    	$('.act').removeClass('sel');
    	$('#ljubljana').addClass('sel');
    	$.ajax({
    		method: 'get',
    		async: 'false',
    		url: "https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where u='c' and woeid in (select woeid from geo.places(1) where text='ljubljana')&format=json&callback=vreme",
    		success: function(result) {
    			$('#userData').show();
		    	var ehr = $('#userEhr').html();
		        readData(ehr);
	    	}
    	});
    });
    
    $('#kairo').click(function kairo() {
    	$('.act').removeClass('sel');
    	$('#kairo').addClass('sel');
    	$.ajax({
    		method: 'get',
    		async: 'false',
    		url: "https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where u='c' and woeid in (select woeid from geo.places(1) where text='cairo')&format=json&callback=vreme",
    		success: function(result) {
		    	$('#userData').show();
		    	var ehr = $('#userEhr').html();
		        readData(ehr);
	    	}
    	});
    });
    
    $('#norilsk').click(function norilsk() {
    	$('.act').removeClass('sel');
    	$('#norilsk').addClass('sel');
    	$.ajax({
    		method: 'get',
    		async: 'false',
    		url: "https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where u='c' and woeid in (select woeid from geo.places(1) where text='norilsk')&format=json&callback=vreme",
    		success: function(result) {
    			$('#userData').show();
		    	var ehr = $('#userEhr').html();
		        readData(ehr);
	    	}
    	});
    });
    
    $('#generate').click(function generate() {
    	$('#places').addClass('collapse');
    	
    	var ehr1 = generirajPodatke(1);
    	var ehr2 = generirajPodatke(2);
    	var ehr3 = generirajPodatke(3);
    	
    	var users = $('#selectUser').html();
    	$('#selectUser').html(users + '<optgroup label="Generirani podatki"><option value="' + ehr1 + '">Tjaša Gal (' + ehr1 + ')</option><option value="' + ehr2 + '">Jakob Muster (' + ehr2 + ')</option><option value="' + ehr3 + '">Jožef Rogelj (' + ehr3 + ')</option></optgroup>');
    	
    	$('#ehrs').modal();
    });
    
    $('#addMeasurement').click(function newMeasurement() {
    	addMeasurement();
    });
});